import { InspirationComponent } from './about/inspiration/inspiration.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PaginationComponent } from './tabs/pagination/pagination.component';
import { MerryGoRoundComponent } from './tabs/merry-go-round/merry-go-round.component';
import { ListComponent } from './tabs/list/list.component';
import { TabsComponent } from './tabs/tabs.component';
import { WhoIAmComponent } from './about/who-i-am/who-i-am.component';
import { AboutComponent } from './about/about.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LyricsComponent } from './lyrics/lyrics.component';
import { WithinTheModalLyricsComponent } from './lyrics/within-the-modal-lyrics/within-the-modal-lyrics.component';
import { SidebarComponent } from './sidebar/sidebar.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    LyricsComponent,
    WithinTheModalLyricsComponent,
    SidebarComponent,
    AboutComponent,
    WhoIAmComponent,
    TabsComponent,
    ListComponent,
    MerryGoRoundComponent,
    PaginationComponent,
    InspirationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
